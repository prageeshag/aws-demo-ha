# Use aws provider 
provider "aws" {
  version = "~> 2.0"
  region  = "us-east-1"
}


## Store terraform state in s3 bucket
terraform {
  backend "s3" {
    bucket = "<s3-bucket-name>"
    key    = "demo-tf-state/dev"
    region = "us-east-1"
  }
}
