# Deploy High Availability Architecture in AWS using Terraform

In here I am deploying 2 webservers in 2 availability zones with sharing same EFS storage.


## Architecure Diagram

![Arcgitecture](./screens/architecture.png)


## Tarraform
I have used a gcs bucket to store terraform state file

Go to terraform folder and execure below commands to provision your resources.

```
terraform init

terraform plan -out=plan.out

terraform apply plan.out
```